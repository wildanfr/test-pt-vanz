<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
class RajaOngkirController extends Controller
{
    public function getProvince()
    {
    	$client = new Client();

    	$res = $client->request(
    		"GET", 
    		"https://api.rajaongkir.com/starter/province",
    		[
    		'headers' => [
    			'key' => '9d8748d6162e855b9a1d067622c9ced6',
    			'Accept' => 'application/json'
    		]
    	]);

    	$rajaOngkir = json_decode($res->getBody(), true);
    	return $rajaOngkir['rajaongkir']['results'];

    }

    public function getCity(Request $request)
    {

    	$client = new Client();

    	$res = $client->request(
    		"GET", 
    		"https://api.rajaongkir.com/starter/city?province={$request->province_id}",
    		[
    		'headers' => [
    			'key' => '9d8748d6162e855b9a1d067622c9ced6',
    			'Accept' => 'application/json'
    		]
    	]);

    	$rajaOngkir = json_decode($res->getBody(), true);
    	return $rajaOngkir['rajaongkir']['results'];
    }

    public function getCost(Request $request)
    {
    	$client = new Client();

    	$res = $client->request(
    		"POST", 
    		"https://api.rajaongkir.com/starter/cost",
    		[
    		'headers' => [
    			'key' => '9d8748d6162e855b9a1d067622c9ced6',
    			'Accept' => 'application/json'
    		],
    		'form_params'=> [
    			'origin'=> 444,
    			'destination'=>$request->city,
    			'weight'=> 1000,
    			'courier'=> $request->courier
    		]
    	]
    );

    	$rajaOngkir = json_decode($res->getBody(), true);
    	return $rajaOngkir['rajaongkir']['results'][0];
    }
}
