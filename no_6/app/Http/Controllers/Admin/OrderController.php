<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
class OrderController extends Controller
{
    public function list_order()
    {
    	$orders = Order::latest()->paginate(10);
    	return view('admin.list_order', compact('orders'));
    }

    public function showlist_order(Order $order)
    {
    	return view('admin.showlist_order', compact('order'));
    }
}
