<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderDetail;

class CheckoutController extends Controller
{
    public function index() 
    {
    	$items = session('cart');
        // dd($items);
    	// $couriers = config('olshop.couriers');
    	// dd($couriers);
    	return view('frontend.checkout.index', compact('items'));  
    }

    public function store(Request $request)
    {

    	$items = collect(session('cart'));

        dd($items);
    	// dd($request->all());

    	$order = Order::create([
    		'user_id'   => auth()->id(),
    		'user_name' => $request->name,
    		'user_address' => $request->address,
    		'user_phone'=>$request->phone,
    		'status'=> 'UINPAID',
    		'courier'=> "{$request->courier} {$request->service}",
    		'shiping_cost'=>'-',
    		'total'=> $items->sum('price')
    	]);

    	$items->each(function($item) use ($order){
    		OrderDetail::create([
    			'order_id' => $order->id,
    			'product_id' => $item['product_id'], 
    			'qty'=> $item['qty'],
    			'price'=>$item['price'],
    			'subtotal'=>$item['qty'] * $item['price']

    		]);
    		
    	});

    	session()->forget('cart');

    	// return view('frontend.checkout.detail',compact('order'));
        return redirect()->route('order.show', compact('order'));

    }
}
