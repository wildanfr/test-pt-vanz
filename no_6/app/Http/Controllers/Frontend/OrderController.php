<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Order;
class OrderController extends Controller
{
    public function index()
    {
    	$orders = Order::ByAuth()->latest()->paginate(10);
    	return view('frontend.order.index', compact('orders'));
    }

    public function show(Order $order)
    {
    	return view('frontend.checkout.detail', compact('order'));
    }
}
