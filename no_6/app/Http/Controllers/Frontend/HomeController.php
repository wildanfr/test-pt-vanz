<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
class HomeController extends Controller
{
   public function index()
   {
   	  $products = Product::paginate(12);
   	  return view('homepage', compact('products'));
   }
}
