<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
class ProductController extends Controller
{
    public function show(Product $product)
    {   
 
    	return view('frontend.product.show', [
    		'product' => $product
    	]);
    }

    public function byCategory(Category $category)
    {
    	$products = $category->products()->paginate(12);
        return view('frontend.product.by-category', [
            'products' => $products, 

        ]);
    }
}
