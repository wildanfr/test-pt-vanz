<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Order extends Model
{
    protected $guarded = [];


    public function scopeByAuth($query)
    {
    	return $query->where('user_id', Auth::user()->id);
    }

    public function orderDetails()
    {
    	return $this->hasMany(orderDetail::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
