@extends('layouts.app')

@section('content')

<div class="columns is-centered">
	<div class="column is-6">
		<h1 class="is-size-2 has-text-centered">Order Detail</h1>
		@if($order->status == "UNPAID")
		<hr>
		<p class="has-text-centered">This is our order detail, please make a payment to this account below to confirm your order</p>
		<br>
		<table class="table is-pulled-right">
			<tr>
				<td class="has-text-right">Bank Name:</td>
				<td> BRI </td>
			</tr>
			<tr>
				<td class="has-text-right">Account Name:</td>
				<td>{{ Auth::user()->name }}</td>
			</tr>
			<tr>
				<td class="has-text-right">Account Number:</td>
				<td>1234567890</td>
			</tr>
			<tr>
				<td class="has-text-right">Amount</td>
				<td>Rp.{{ format_rupiah($order->total + $order->shiping_cost) }}</td>
			</tr>
		</table>
		@elseif($order->status == "PAID")

		<table class="table is-pulled-right">
			<tr>
				<td class="has-text-right">Order Status</td>
				<td> {{ $order->status }}</td>
			</tr>
			<tr>
				<td class="has-text-right">Amount</td>
				<td>Rp.{{ format_rupiah($order->total + $order->shiping_cost) }}</td>
			</tr>
		</table>

		@endif


		<table class="table is-bordered is-fullwidth">
			<tr style="background-color: #3498DB;">
				<th style="color: white;">No</th>
				<th style="color: white;">Item Name</th>
				<th style="color: white;">Price</th>
			</tr>
			{{-- @php $no = 1; @endphp --}}
			@foreach($order->orderDetails as $item)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $item->product->name }}</td>
				<td class="has-text-right">Rp.{{ format_rupiah($item->product->price) }}</td>
			</tr>
			@endforeach

			<tr>
				<td colspan="2" class="has-text-right">Shipping Cost</td>
				<td class="has-text-right">-</td>
			</tr>
			<tr>
				<td colspan="2" class="has-text-right">Total</td>
				<td class="has-text-right">Rp.{{ format_rupiah($order->total) }}</td>
			</tr>
		</table>
	</div>
</div>

@endsection