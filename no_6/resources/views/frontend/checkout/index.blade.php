@extends('layouts.app')
@section('content')
<div class="container">
	<h1 class="is-size-2">Checkout</h1>
<div class="columns">
	@if($items)
	
	<div class="column is-8">
		<h1 class="is-size-5">Shipping Address</h1>
		<form method="POST" action="{{ route('checkout.store') }}">
		    @csrf
		    <div class="field">
		        <label for="" class="label">Name</label>
		        <div class="control">
		            <input type="text" name="name" class="input @error('name') is-danger @enderror" value="{{ auth()->user()->name ?? old('name') }}" placeholder="Enter your name" required>
		            @error('name')
		            <span class="invalid-feedback" role="alert">
		                <strong class="help is-danger">{{ $message }}</strong>
		            </span>
		            @enderror
		        </div>
		    </div>
		    <div class="field">
		        <label for="" class="label">Email</label>
		        <div class="control">
		            <input type="email" name="email" class="input @error('email') is-danger @enderror"  placeholder="Enter your Email" value="{{ auth()->user()->email ?? old('email') }}" required>
		            @error('email')
		            <span class="invalid-feedback" role="alert">
		                <strong class="help is-danger">{{ $message }}</strong>
		            </span>
		            @enderror
		        </div>
		    </div>
		    <div class="field">
		        <label for="" class="label">Province</label>
		        <div class="control">
		          	<div class="select is-fullwidth">
		          		<select name="province" id="province" class="{{ $errors->has('province') ? 'is-danger' :'' }}">
		          			<option value="">Select a Province</option>
		          			<option value="">Jawa Barat</option>
		          		</select>
		          	</div>
		            @error('province')
		            <span class="invalid-feedback" role="alert">
		                <strong class="help is-danger">{{ $message }}</strong>
		            </span>
		            @enderror
		        </div>
		    </div>

		    <div class="field">
		        <label for="" class="label">City</label>
		        <div class="control">
		          	<div class="select is-fullwidth">
		          		<select name="city" id="city"class="{{ $errors->has('city') ? 'is-danger' :'' }}">
		          			<option value="">Select a City</option>
		          			<option value="Karawang">Karawang</option>
		          		</select>
		          	</div>
		            @error('city')
		            <span class="invalid-feedback" role="alert">
		                <strong class="help is-danger">{{ $message }}</strong>
		            </span>
		            @enderror
		        </div>
		    </div>
		    <div class="field">
		        <label for="" class="label">Address</label>
		        <div class="control">
		            <textarea class="textarea" name="address"  placeholder="Enter your address" class="input @error('address') is-danger @enderror" value="{{ old('address') }}" required></textarea>
		            @error('address')
		            <span class="invalid-feedback" role="alert">
		                <strong class="help is-danger">{{ $message }}</strong>
		            </span>
		            @enderror
		        </div>
		    </div>

		    <div class="field">
		        <label for="" class="label">Courier</label>
		        <div class="control">
		          	<div class="select is-fullwidth">
		          		<select name="courier" id="courier"class="{{ $errors->has('courier') ? 'is-danger' :'' }}">
		          			<option value="">Select a Courier</option>
		          			<option value="jne">JNE</option>
		          			<option value="tiki">Tiki</option>
		          			<option value="pos">POS Indonesia</option>
		          		</select>
		          	</div>
		            @error('courier')
		            <span class="invalid-feedback" role="alert">
		                <strong class="help is-danger">{{ $message }}</strong>
		            </span>
		            @enderror
		        </div>
		    </div>

		    <div class="field">
		        <label for="" class="label">Service</label>
		        <div class="control">
		          	<div class="select is-fullwidth">
		          		<select name="service" id="service"class="{{ $errors->has('service') ? 'is-danger' :'' }}">
		          			<option value="">Select a Service</option>
		          			<option value="Ready">Ready</option>
		          			
		          		</select>
		          	</div>
		            @error('service')
		            <span class="invalid-feedback" role="alert">
		                <strong class="help is-danger">{{ $message }}</strong>
		            </span>
		            @enderror
		        </div>
		    </div>
		    <div class="field">
		        <label for="" class="label">Phone</label>

		        <div class="control">
		         <input id="phone" type="number" name="phone"  placeholder="Enter your phone" class="input @error('phone') is-danger @enderror" value="{{ old('phone') }}" required>
		         @error('phone')
		         <span class="invalid-feedback" role="alert">
		             <strong class="help is-danger">{{ $message }}</strong>
		         </span>
		         @enderror
		     </div>
		 </div>
		 <input type="text" id="shiping" style="display: none;" name="shiping" value="test"  placeholder="Enter your phone" class="input @error('shiping') is-danger @enderror" value="{{ old('shiping') }}" required>
		    <div class="form-group row mb-0">
		        <div class="col-md-6 offset-md-4">
		            <button type="submit" class="button is-info">
		            {{ __('Save') }}
		            </button>
		        </div>
		    </div>
		</form>
		{{-- <h1 class="is-size-5">Shopping Cart</h1> --}}
		
	</div>
	<div class="column is-4">
		<h1 class="is-size-5">Cart Detail</h1>
		@php

			$totalItems = 0;
			$totalPrice = 0;


		@endphp
		@foreach($items as $key => $item)
		@php

		$totalItems += $item['qty'];
		$totalPrice += $item['price'];

		@endphp
		<div class="card">
			<div class="card-content">
				<div class="content">
				<div class="columns">
					<div class="column is-3">
						<img src="{{ $item['image'] }}" class="image is-64x64">
					</div>
					<div class="column is-9">
						<p>
							{{ $item['name'] }} <br>
							Rp.{{ format_rupiah($item['price']) }}
							
						</p>
						{{-- <p class="has-text-danger is-size-3">Rp.{{ $item['formatted_price'] }}</p> --}}
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="card-action">

		</div> 
		@endforeach 
		<div class="card">
			<div class="card-content">
				<p>Total Items 		: <span class="is-pulled-right">{{ $totalItems }} Items</span> </p>
				<p>ETD		  	    : <span class="is-pulled-right" id="etd">-</span> </p>
				<p>Shipping Cost	: <span class="is-pulled-right" id="shipping_cost">-</span> </p>
				<p>Total Price      : <span class="is-pulled-right" id="total_price">Rp.{{ format_rupiah($totalPrice) }}</span></p>

				<hr>
				<p>Grand Total : <span class="is-pulled-right" id="grand_total">Rp.-</span></p>
			</div>
		</div>
		</div>
	</div>
	@else
	<div class="card">
		<div class="card-content">
			<div class="content">
				<h2 class="is-size-5">No Item Cart</h2>
			</div>
		</div>
	</div>
	@endif
</div>
</div>
@endsection
@push('scripts')

<script>

	function convertToRupiah(angka)
	{
		var rupiah = '';		
		var angkarev = angka.toString().split('').reverse().join('');
		for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
		return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
	}
	/**
	 * Usage example:
	 * alert(convertToRupiah(10000000)); -> "Rp. 10.000.000"
	 */
	 
	function convertToAngka(rupiah)
	{
		return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
	}

	$.ajax({
		type: 'GET',
		url: "{{ route('rajaongkir.province') }}",
		success: function(data){

			var provinces = data; 
			provinces.forEach(function(province){

				// var provinsi = new Option(province.province_id, province.province)

				// $(provinsi).html(province.province)

				// $('#province').append(provinsi)
				$('#province').append('<option value="'+province.province_id+'">'+ province.province +'</option>')
			});
		}


	});

 //  $(document).ready(function() {
 //  	$('#province').change(function() {
	// 	var provinceId = $('#province').val();

	// 	$.ajax({
	// 		type: 'GET',
	// 		url: "{{ route('rajaongkir.city') }}",
	// 		data: 'province_id=' + provinceId,
	// 		success: function(data){
	// 			var cities = data;
	// 			$('#city').empty().append('<option>Select a City</option>');
				
	// 			cities.forEach(function(city){

	// 				$('#city').append('<option value="'+city.city_id+'">'+ city.city_name +'</option>')
	// 			});
	// 		}
	// 	});
	// });

	 //  	$('#courier').change(function() {

	 //  		var cityId = $('#city').val();
	 //  		var courier = $('#courier').val();

		// 	$.ajax({
		// 		type: 'POST',
		// 		url: "{{ route('rajaongkir.cost') }}",
		// 		data: {
		// 			city: cityId,
		// 			courier:courier
		// 		},
		// 		success: function(data){
					
		// 			var couriers = data.costs
		// 			// console.log(couriers)
		// 			$('#service').empty().append('<option>Select a Service</option>');
					
		// 			couriers.forEach(function(courier){

		// 				$('#service').append('<option value="'+courier.service+'">'+ courier.description + '('+ courier.service + ') - ( Rp.'+ courier.cost[0].value +')</option>')
		// 			});
		// 		}
		// 	});
		// });


		 //  	$('#service').change(function() {

		 //  		var cityId = $('#city').val();
		 //  		var courier = $('#courier').val();
		 //  		var service = $('#service ').val();



			// 	$.ajax({
			// 		type: 'POST',
			// 		url: "{{ route('rajaongkir.cost') }}",
			// 		data: {
			// 			city: cityId,
			// 			courier:courier
			// 		},
			// 		success: function(data){
						
			// 			var couriers = data.costs
					


			// 			var shipCost = couriers.find(function(cost){
			// 				return cost.service == service
			// 			});

			// 			var totalPrice = convertToAngka($('#total_price').text())

			// 			var shippingCost = shipCost.cost[0].value;
			// 			var grandTotal = totalPrice + shippingCost;


			// 			// console.log(grandTotal)

			// 			$('#shipping_cost').text(convertToRupiah(shippingCost))
			// 			$('#shiping').val(shippingCost)
			// 			$('#etd').text(shipCost.cost[0].etd + " days")
			// 			$('#grand_total').text(convertToRupiah(grandTotal))

						
			// 		}
			// 	});
			// });
  // });
	
</script>

@endpush
