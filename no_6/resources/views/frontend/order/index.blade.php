@extends('layouts.app')

@section('content')

<div class="columns is-centered">
	<div class="column is-6">
		<h1 class="is-size-2 has-text-centered">Transaction History</h1>
		<hr>
		<p class="has-text-centered">This is your transaction history</p>
		<br>
		<table class="table is-bordered is-fullwidth">
			<tr style="background-color: #3498DB;">
				<th style="color: white;">No</th>
				<th style="color: white;">Order ID</th>
				<th style="color: white;">Status</th>
				<th style="color: white;">Total Price</th>
			</tr>
			{{-- @php $no = 1; @endphp --}}
			@foreach($orders as $order)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>-</td>
				<td>{{ $order->status }}</td>
				<td class="has-text-right">Rp.{{ format_rupiah($order->total) }}</td>
			</tr>
			@endforeach
		</table>
		 <div class="columns is-centered">
		 	{{  $orders->links('vendor.pagination.bulma') }}
		 </div>
			
	</div>
</div>

@endsection