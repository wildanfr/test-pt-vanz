<div class="column is-2">
	<div class="card">
		<div class="card-image">
			<figure class="image is-4by3">
				<img src="{{ $product->getImage() }}" alt="">
			</figure>
		</div>
		<div class="card-content">
			<div class="content">
				<h6><a href="{{ route('frontend.product.show', $product) }}" title="">{{ $product->name }}</a></h6>
				<p> {{ $product->description }} </p>
				<p class="has-text-danger">Rp.{{ $product->getPrice() }}</p>
			</div>
			<a href="{{ route('cart.add.item', $product) }}"  class="button is-primary" title="">Add to cart</a>
		</div>
	</div>
</div>
