@extends('layouts.app')
@section('content')
<div class="container">
    <div class="columns is-centered">
        <div class="column is-half box">
            
            <h1 class="is-size-3">Register</h1>
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="field">
                    <label for="" class="label">Name</label>
                    <div class="control">
                        <input type="text" name="name" class="input @error('name') is-danger @enderror" value="{{ old('name') }}" placeholder="Enter your name" required>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong class="help is-danger">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="field">
                    <label for="" class="label">Email</label>
                    <div class="control">
                        <input type="email" name="email" class="input @error('email') is-danger @enderror"  placeholder="Enter your Email" value="{{ old('email') }}" required>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong class="help is-danger">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="field">
                    <label for="" class="label">Password</label>
                    <div class="control">
                        <input  type="password" name="password"  placeholder="Enter your password" class="input @error('password') is-danger @enderror" value="{{ old('password') }}" required>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong class="help is-danger">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="field">
                    <label for="" class="label">Confirm Password</label>
                    <div class="control">
                        <input id="password_confirmation" type="password"  placeholder="Enter your password confirm" name="password_confirmation" class="input @error('password_confirmation') is-danger @enderror" value="{{ old('password_confirmation') }}" required>
                        @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong class="help is-danger">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="field">
                    <label for="" class="label">Address</label>
                    <div class="control">
                        <input id="address" type="text" name="address"  placeholder="Enter your address" class="input @error('address') is-danger @enderror" value="{{ old('address') }}" required>
                        @error('address')
                        <span class="invalid-feedback" role="alert">
                            <strong class="help is-danger">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="field">
                    <label for="" class="label">Phone</label>

                    <div class="control">
                     <input id="phone" type="number" name="phone"  placeholder="Enter your phone" class="input @error('phone') is-danger @enderror" value="{{ old('phone') }}" required>
                     @error('phone')
                     <span class="invalid-feedback" role="alert">
                         <strong class="help is-danger">{{ $message }}</strong>
                     </span>
                     @enderror
                 </div>
             </div>
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="button is-info">
                        {{ __('Register') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
@endsection