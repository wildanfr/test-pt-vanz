@extends('layouts.app')

@section('content')
<div class="container">
    <div class="columns is-centered">
        <div class="column is-half box">
       <h1 class="is-size-3">Reset Password</h1>
           

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="field">
                            <label for="" class="label">Email</label>
                            <div class="control">
                                <input type="email" name="email" class="input @error('email') is-danger @enderror"  placeholder="Enter your Email" value="{{ old('email') }}" required>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="help is-danger">{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="field">
                         <label for="" class="label">Password</label>
                         <div class="control">
                             <input  type="password" name="password"  placeholder="Enter your password" class="input @error('password') is-danger @enderror" value="{{ old('password') }}" required>
                             @error('password')
                             <span class="invalid-feedback" role="alert">
                                 <strong class="help is-danger">{{ $message }}</strong>
                             </span>
                             @enderror
                         </div>
                     </div>

                     <div class="field">
                        <label for="" class="label">Confirm Password</label>
                        <div class="control">
                            <input id="password_confirmation" type="password"  placeholder="Enter your password confirm" name="password_confirmation" class="input @error('password_confirmation') is-danger @enderror" value="{{ old('password_confirmation') }}" required>
                            @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                <strong class="help is-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="button is-info">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
