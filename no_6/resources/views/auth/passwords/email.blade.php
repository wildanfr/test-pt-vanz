@extends('layouts.app')

@section('content')
<div class="container">
    <div class="columns is-centered">
        <div class="column is-half box">
           <h1 class="is-size-half">Reset Password</h1>
                    @if (session('status'))
                        <div class="notification is-info" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="field">
                          <label for="" class="label">Email</label>
                          <div class="control">
                              <input type="email" name="email" class="input @error('email') is-danger @enderror"  placeholder="Enter your Email" value="{{ old('email') }}" required>
                              @error('email')
                              <span class="invalid-feedback" role="alert">
                                  <strong class="help is-danger">{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                      </div>

                        <div class="field">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="button is-info">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection
