@extends('admin.templates.default')


@section('content')

  <section class="content-header">
    <h1>
      Orders
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><i class="glyphicon glyphicon-info-sign"></i> Orders</li>
      <li class="active"><i class="fa fa-files-o"></i> Daftar Order Detail </li>
    </ol> 
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row" id="field_detail">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><b>Daftar Order Detail</b></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>

          </div>
            @include('admin.templates.partials._alert')

          <!-- /.box-header -->
          <div class="box-body"> 

          <h4>Order ID : {{ $order->id }}</h4> 
          <h4>Order Status : {{ $order->status }}</h4>  
          <h4>Total : {{ format_rupiah($order->total) }}</h4> 
          <hr>     
         <table id="tblDetail" class="table table-striped table-condensed table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th >No</th>
                  <th >Item Name</th> 
                  <th >Price</th>
                </tr>
              </thead>
              <tbody>
                @php $no = 1; @endphp
                @foreach($order->orderDetails as $item)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $item->product->name }}</td>
                  <td class="text-right">{{ format_rupiah($item->product->price) }}</td>
                </tr>
                @endforeach
                <tr>
                  <td colspan="2"><p class="text-right">Shiping Cost</p></td>
                  <td><p class="text-right">-</p></td>
                </tr>
                <tr>
                  <td colspan="2"><p class="text-right">Grand Total</p></td>
                  <td><p class="text-right">{{ format_rupiah($order->total) }}</p></td>
                </tr>
              </tbody>
            </table>      
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
        {{--     {{ $order->links('vendor.pagination.adminlte') }} --}}
          </div>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
@endsection

