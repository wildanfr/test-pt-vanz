@extends('admin.templates.default')


@section('content')

  <section class="content-header">
    <h1>
      Orders
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><i class="glyphicon glyphicon-info-sign"></i> Orders</li>
      <li class="active"><i class="fa fa-files-o"></i> Daftar Order </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row" id="field_detail">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><b>Daftar Order</b></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>
            <hr>
            {{-- <a class="btn btn-primary" href="">
              <span class="fa fa-plus"></span> Add Order
            </a> --}}
          </div>
            @include('admin.templates.partials._alert')

          <!-- /.box-header -->
          <div class="box-body">
         <table id="tblDetail" class="table table-striped table-condensed table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th style='width: 1%;text-align: center'>No</th>
                  <th style='width: 3%;text-align: center'>Order ID</th>
                  <th style='width: 3%;text-align: center'>Status</th>
                  <th style='width: 3%;text-align: center'>Courier</th>
                  <th style='width: 3%;text-align: center'>Total</th>
                </tr>
              </thead>
              <tbody>
                @php $no = 1; @endphp
                @foreach($orders as $order)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td><a href="{{ route('order.showlist_order', $order->id) }}">{{ $order->id }}</a></td>
                  <td>{{ $order->status }}</td>
                  <td>{{ $order->courier }}</td>
                  <td>Rp.{{ format_rupiah($order->total) }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            {{ $orders->links('vendor.pagination.adminlte') }}
          </div>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
@endsection
