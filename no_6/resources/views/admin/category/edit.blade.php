@extends('admin.templates.default')


@section('content')
<section class="content-header">
	<h1>
		Category Product
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><i class="glyphicon glyphicon-info-sign"></i> Category</li>
		<li><a href="#"><i class="fa fa-files-o"></i> Category </a></li>
		<li class="active">Add Category Product</li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Add Category Product</h3>
		</div>
		<div class="box-body">
			<div class="form-group">
				<form  method="post" action="{{ route('category.update', $category->id) }}">
					@csrf
					@method('PUT')
					<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name" class="col-md-2 control-label">Name</label>
						<div class="col-md-4">
							<input type="text" name="name" value="{{ $category->name }}"  placeholder="Name product" class="form-control">
							{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
						</div>
					</div>
					<br>
					<br>
					<br>
					<br>
					<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
						<label for="slug" class="col-md-2 control-label">Description</label>
						<div class="col-md-4">
							<textarea name="description" rows="2" placeholder="Description" class="form-control">{{ $category->description }}</textarea>
							{!! $errors->first('description', '<p class="help-block">:message</p>') !!}
						</div>
					</div>
					<br>
					<br>
					<br>
					<br>
					<div class="box-footer">
						<div class="form-group">
							<div class="col-md-4 col-md-offset-2"> 
								<button type="submit" class="btn btn-primary">Update</button>
								<a class="btn btn-default" href="{{ route('category.index') }}"> Cancel </a> 
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>
	</section>
	@endsection