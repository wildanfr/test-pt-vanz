@extends('admin.templates.default')


@section('content')

  <section class="content-header">
    <h1>
      Category
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><i class="glyphicon glyphicon-info-sign"></i> Category</li>
      <li class="active"><i class="fa fa-files-o"></i> Daftar Category </li>
    </ol> 
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row" id="field_detail">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><b>Daftar Category Product</b></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>
            <hr>
            <a class="btn btn-primary" href="{{ route('category.create') }}">
              <span class="fa fa-plus"></span> Add Category
            </a>
          </div>
            @include('admin.templates.partials._alert')

          <!-- /.box-header -->
          <div class="box-body">         
         <table id="tblDetail" class="table table-striped table-condensed table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th style='width: 1%;text-align: center'>No</th>
                  <th style='width: 5%;text-align: center'>Name</th>                
                  <th style='width: 5%;text-align: center'>Slug </th>
                  <th style='width: 10%;text-align: center'>Description</th>
                  <th style='width: 2%;text-align: center'>Action</th>
                </tr>
              </thead>
              <tbody>
               {{--  @php $no = 1; @endphp --}}
                @foreach($categories as $category)
                <tr>
                  <td>#</td>
                  <td>{{ $category->name }}</td>
                  <td>{{ $category->slug }}</td>
                  <td>{{ $category->description }}</td>
                  <td>
                    <a style="text-align: center" href="{{ route('category.edit', $category) }}" class="btn btn-sm btn-warning" title=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                   <button id="delete" data-title="{{ $category->name }}" href="{{ route('category.destroy', $category) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                  </td>
                  <form action="" method="post" value="" id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <input type="submit" name="" style="display: none;">
                  </form>
                </tr>
                @endforeach
              </tbody>
            </table>      
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            {{ $categories->links('vendor.pagination.adminlte') }}
          </div>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
@endsection
@push('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
  $('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');

    swal({
      title: "Are you sure to delete this data "+ title +" category?",
      text: "Once deleted, you will not be able to recover this category!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        document.getElementById('deleteForm').action = href;
        document.getElementById('deleteForm').submit();
        swal("Your category has been deleted!", {
          icon: "success",
        });
      } 
    });
  });
</script>
@endpush