@extends('admin.templates.default')
@section('content')
<section class="content-header">
	<h1>
	Product
	<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><i class="glyphicon glyphicon-info-sign"></i> Product</li>
		<li><a href="#"><i class="fa fa-files-o"></i> Daftar Product </a></li>
		<li class="active">Add a Product</li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Add a Product</h3>
		</div>
		<div class="box-body">
			<div class="form-group">
				<form  method="post" action="{{ route('product.store') }}" enctype="multipart/form-data">
					@csrf
					<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name" class="col-md-2 control-label">Name</label>
						<div class="col-md-4">
							<input type="text" name="name" value="{{ old('name') }}" placeholder="Name product" class="form-control">
							{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
						</div>
					</div>
					<br>
					<br>
					<br>
					<br>
					<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
						<label for="slug" class="col-md-2 control-label">Description</label>
						<div class="col-md-4">
							<textarea name="description" rows="2" placeholder="Description" class="form-control"></textarea>
							{!! $errors->first('description', '<p class="help-block">:message</p>') !!}
						</div>
					</div>
					<br>
					<br>
					<br>
					<br>
					<div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
						<label for="price" class="col-md-2 control-label">Price</label>
						<div class="col-md-4">
							<input type="text" name="price" placeholder="Price product" class="form-control">
							{!! $errors->first('price', '<p class="help-block">:message</p>') !!}
						</div>
					</div>
					<br>
					<br>
					<br>
					<br>
					<div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
						<label for="image" class="col-md-2 control-label">Image</label>
						<div class="col-md-4">
							<input type="file" name="image" placeholder="Image product" class="form-control">
							{!! $errors->first('image', '<p class="help-block">:message</p>') !!}
						</div>
					</div>
					<br>
					<br>
					<br>
					<br>
					<div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
						<label for="category" class="col-md-2 control-label">category</label>
						<div class="col-md-4">
							<select name="category[]" class="form-control select2" multiple="multiple">
								@foreach($categories as $category)
								<option value="{{ $category->id }}">{{ $category->name }}</option>
								@endforeach
							</select>
							{!! $errors->first('category', '<p class="help-block">:message</p>') !!}
						</div>
					</div>
					<br>
					<br>
					<br>
					<br>
					<div class="box-footer">
						<div class="form-group">
							<div class="col-md-4 col-md-offset-2">
								<button type="submit" class="btn btn-primary">Save</button>
								<a class="btn btn-default" href="{{ route('category.index') }}"> Cancel </a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	@endsection
	@push('select2styles')
	<link rel="stylesheet" href="{{asset('assets_/bower_components/select2/dist/css/select2.min.css')}}">
	@endpush

	@push('scripts')
	<script type="text/javascript" src="{{asset('assets_/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
	<script type="text/javascript">
		$(function(){

			$('.select2').select2()

		});
	</script>
	@endpush