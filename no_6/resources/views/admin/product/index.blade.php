@extends('admin.templates.default')


@section('content')

  <section class="content-header">
    <h1>
      Product
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><i class="glyphicon glyphicon-info-sign"></i> Product</li>
      <li class="active"><i class="fa fa-files-o"></i> Daftar Product </li>
    </ol> 
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row" id="field_detail">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><b>Daftar Product Product</b></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>
            <hr>
            <a class="btn btn-primary" href="{{ route('product.create') }}">
              <span class="fa fa-plus"></span> Add product
            </a>
          </div>
            @include('admin.templates.partials._alert')

          <!-- /.box-header -->
          <div class="box-body">         
         <table id="tblDetail" class="table table-striped table-condensed table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th style='width: 1%;text-align: center'>No</th>
                  <th style='width: 5%;text-align: center'>Name</th> 
                  <th style='width: 5%;text-align: center'>Image Product</th>               
                  <th style='width: 7%;text-align: center'>Slug </th>
                  <th style='width: 10%;text-align: center'>Description</th>
                  <th style='width: 5%;text-align: center'>Category </th>
                  <th style='width: 4%;text-align: center'>Price </th>
                  <th style='width: 2%;text-align: center'>Action</th>
                </tr>
              </thead>
              <tbody>
               {{--  @php $no = 1; @endphp --}}
                @foreach($products as $product)
                <tr>
                  <td>#</td>
                  <td>{{ $product->name }}</td>
                  <td>
                    <center>
                      <img src="{{ $product->getImage() }}" alt="" height="80px" width="100px">
                    </center>
                  </td>
                  <td>{{ $product->slug }}</td>
                  <td>{{ $product->description }}</td>
                  <td>
                    @foreach($product->categories as $category)
                      <span class="label label-primary">{{ $category->name }}</span>
                    @endforeach  
                  </td>
                  <td>{{ $product->price }}</td>
                  <td>
                    <a style="text-align: center" href="{{ route('product.edit', $product) }}" class="btn btn-xs btn-warning" title=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>
                   <button id="delete" data-title="{{ $product->name }}" href="{{ route('product.destroy', $product) }}" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
                  </td>
                  <form action="" method="post" value="" id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <input type="submit" name="" style="display: none;">
                  </form>
                </tr>
                @endforeach
              </tbody>
            </table>      
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            {{ $products->links('vendor.pagination.adminlte') }}
          </div>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
@endsection
@push('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
  $('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');

    swal({
      title: "Are you sure to delete this data "+ title +" Product?",
      text: "Once deleted, you will not be able to recover this Product!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        document.getElementById('deleteForm').action = href;
        document.getElementById('deleteForm').submit();
        swal("Your category has been deleted!", {
          icon: "success",
        });
      } 
    });
  });
</script>
@endpush