<?php


//

Route::get('/', function() {
    return view("admin.index");
})->name('admin.index');

Route::resource('/category', 'CategoryController');
Route::resource('/product', 'ProductController');
Route::get('/order', 'OrderController@list_order')->name('order.list_order');
Route::get('/order/{order}', 'OrderController@showlist_order')->name('order.showlist_order');
