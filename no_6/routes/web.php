 <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| 
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','Frontend\\HomeController@index')->name('homepage');
Route::get('/product/{product}','Frontend\\ProductController@show')->name('frontend.product.show');

Route::get('/product/category/{category}','Frontend\\ProductController@byCategory')->name('frontend.product.bycategory');
Route::get('/cart/checkout', 'Frontend\\CheckoutController@index')->middleware('auth')->name('checkout.index');
Route::post('/cart/checkout', 'Frontend\\CheckoutController@store')->middleware('auth')->name('checkout.store');
Route::get('/cart/{product}', 'Frontend\\CartController@addItem')->middleware('auth')->name('cart.add.item');
Route::get('/cart', 'Frontend\\CartController@index')->middleware('auth')->name('cart.index');
Route::get('/order', 'Frontend\\OrderController@index')->middleware('auth')->name('order.index');

Route::get('/order/{order}', 'Frontend\\OrderController@show')->middleware('auth')->name('order.show');





Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin/login', function() {
    return view('admin.login');
});



 