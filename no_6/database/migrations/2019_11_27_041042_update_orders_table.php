<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn([
                'product_id',
                'product_name',
                'product_description',
                'product_price',
                'qty',
                'subtotal'

            ]);

            $table->integer('total')->after('status');
            $table->string('courier')->after('status');
            $table->string('shiping_cost')->after('courier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['total','courier','shiping_cost']);
        });
    }
}
