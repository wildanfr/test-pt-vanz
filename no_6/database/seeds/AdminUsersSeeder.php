<?php

use Illuminate\Database\Seeder;
use App\Models\User;
class AdminUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
        	'name'  => 'Admin Olshop',
        	'email' => 'admin@olshop.com',
        	'password'=> bcrypt('123456'),
        	'address' => 'Karawang',
        	'phone' => '089652325523'

        ]);

        $admin->assignRole('admin');
    }
}
