<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Category;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
        	'name'  => 'Midrange',
        	'slug'	=> str::slug('Midrange'),
        	'description' => 'Flagship Phone'
        ]);

        Category::create([
        	'name'  => 'Flagship',
        	'slug'	=> str::slug('Flagship'),
        	'description' => 'Flagship Phone'
        ]);

        Category::create([
        	'name'  => 'Low End',
        	'slug'	=> str::slug('Low End'),
        	'description' => 'Flagship Phone'
        ]);

        Category::create([
        	'name'  => 'Asus',
        	'slug'	=> str::slug('Asus'),
        	'description' => 'Asus Phone'
        ]);

        Category::create([
        	'name'  => 'Samsung',
        	'slug'	=> str::slug('Samsung'),
        	'description' => 'Samsung Phone'
        ]);

        Category::create([
        	'name'  => 'Oppo',
        	'slug'	=> str::slug('Oppo'),
        	'description' => 'Oppo Phone'
        ]);

        Category::create([
        	'name'  => 'Vivo',
        	'slug'	=> str::slug('Vivo'),
        	'description' => 'Vivo Phone'
        ]);
    }
}
