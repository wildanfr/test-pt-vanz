-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for db_test
CREATE DATABASE IF NOT EXISTS `db_test` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_test`;

-- Dumping structure for table db_test.fruit
CREATE TABLE IF NOT EXISTS `fruit` (
  `fruit_id` int(11) NOT NULL AUTO_INCREMENT,
  `fruit_name` varchar(15) DEFAULT NULL,
  `fruit_type` enum('IMPORT','LOCAL') DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  PRIMARY KEY (`fruit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table db_test.fruit: ~7 rows (approximately)
DELETE FROM `fruit`;
/*!40000 ALTER TABLE `fruit` DISABLE KEYS */;
INSERT INTO `fruit` (`fruit_id`, `fruit_name`, `fruit_type`, `stock`) VALUES
	(1, 'APEL', 'IMPORT', 10),
	(2, 'KURMA', 'IMPORT', 20),
	(3, 'APEL', 'IMPORT', 50),
	(4, 'MANGGIS', 'LOCAL', 100),
	(5, 'JERUK BALI', 'LOCAL', 10),
	(6, 'KURMA', 'IMPORT', 20),
	(7, 'SALAK', 'LOCAL', 150);
/*!40000 ALTER TABLE `fruit` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
