<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FruitAndi;
class FruitAndiController extends Controller
{
    public function case1_2()
    {
      $data = FruitAndi::all();
      $array = [];
      foreach ($data as $key => $value) {
         $array[] = [
           "fruitId"    => $value->fruit_id,
           "fruitName"  => $value->fruit_name,
           "fruitType"  => $value->fruit_type,
           "stock"      => $value->stock
         ];
      }
      // return json_encode($array);

      // CASE 1 (NO 1-4)

      // no 1 . Buah apa saja yang dimiliki Andi? (fruitName)
      $fruitNameAndi = FruitAndi::select('fruit_name')->get();


      //no 2 Andi memisahkan buahnya menjadi beberapa wadah berdasarkan tipe buah
     //(fruitType). Berapa jumlah wadah yang dibutuhkan? Dan ada buah apa saja di
     // masing-masing wadah?

      $fruitType = FruitAndi::select('fruit_type')->groupBy('fruit_type')->get();
      $fruitCountWadah = $fruitType->count();
      // return json_encode($fruitCountWadah);
      // result : 2 wadah

      //buah pada masing-masing wadah
      $fruitName = FruitAndi::select('fruit_type','fruit_name')->get();
      $groupBy = $fruitName->groupBy('fruit_type');
       // return json_encode($groupBy);



      // no 3 Berapa total stock buah yang ada di masing-masing wadah?
       $fruitAllStock = FruitAndi::select('fruit_type', \DB::raw('SUM(stock) as stock'),)->groupBy('fruit_type')->get();
        // return json_encode($fruitAllStock);

      // No 4 Apakah ada komentar terkait kasus di atas?
      // TIDAK ADA



      // case 2 No 5. Buatlah fungsi untuk menghitung total komentar yang ada, termasuk semua
      //balasan komentar. Berdasarkan data di atas, total komentar adalah 7 komentar.
      $data = Comment::where('comment_content')->count();

    }
}
