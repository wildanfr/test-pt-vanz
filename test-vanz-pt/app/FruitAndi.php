<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FruitAndi extends Model
{
  // protected $connection = 'db_test';

  protected $table = 'fruit';
  protected $primaryKey = 'fruit_id';
  public $timestamps = false;
}
